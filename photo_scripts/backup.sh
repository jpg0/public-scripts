#!/bin/bash

set -x

IFS=$'\n' #split loops on newlines

BACKUP_TARGETS="./backup1
./backup2"

SOURCE_DIR="$1"

function rotate_photo { #rotation, file
    jpegtran -copy all -rotate $1 "$2" "$2.rotated"
    if [ -f "$2.rotated" ]; then
        mv "$2" /tmp
        mv "$2.rotation" "$2"
    else
        echo "Failed to rotate file $2; no rotated file found!"
    fi
}


#first rename & rotate
for photo in `find "$SOURCE_DIR" -iname *.jpg`; do
    # first rotate if necessary
    ORIENTATION=`exiftool -Orientation -n -s3 "$photo"`
    case $ORIENTATION in
        1)
        #No rotation; ignore
        ;;
        8)
        rotate_photo 270 "$photo"
        ;;
        6)
        rotate_photo 90 "$photo"
        ;;
        3)
        rotate_photo 180 "$photo"
        ;;
        *)
        echo "Unknown rotation of $ORIENTATION from exiftool for file $photo"
        exit -2
        ;;
    esac

    #then rename
    exiftool -v '-Filename<${datetimeoriginal}.%e' -d '%Y-%m-%d %H-%M-%S' "$photo"

    #DATETIME=`exiftool -d "YYYY-mm-dd HH-MM-SS" -s3 -DateTimeOriginal "$photo"`\
    #echo $DATETIME
done

#sync to all targets
for target in $BACKUP_TARGETS; do
    rsync -av --delete "$SOURCE_DIR" "$target";
done